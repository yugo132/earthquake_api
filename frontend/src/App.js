import React, { useState, useEffect } from 'react';
import axios from 'axios';
import EarthquakeList from './EarthquakeList';

function App() {
    const [earthquakes, setEarthquakes] = useState([]);
    const [filterInfo, setFilterInfo] = useState({ mag_type: [] });
    const [paginationInfo, setPaginationInfo] = useState({
        currentPage: 1,
        totalItems: 0,
        itemsPerPage: 100
    });

    const fetchData = (params) => {
        axios.get('http://localhost:3000/api/features', { params })
            .then(response => {
                setEarthquakes(response.data.data);
                setPaginationInfo({
                    currentPage: response.data.pagination.current_page,
                    totalItems: response.data.pagination.total,
                    itemsPerPage: response.data.pagination.per_page
                });
                setFilterInfo({
                  mag_type: params['mag_type'] ? params['mag_type'] : []
                });
            })
            .catch(error => {
                console.error('Error fetching earthquakes:', error);
            });
    };

    useEffect(() => {
        fetchData({ per_page: 100, page: 1, mag_type: [] });
    }, []);

    return (
        <div>
            <h1 style={{ margin: '20px 40px', display: 'flex' }}>Earthquake List</h1>

            <EarthquakeList
                data={earthquakes}
                paginationInfo={paginationInfo}
                filterInfo={filterInfo}
                fetchData={fetchData}
            />
        </div>
    );
}

export default App;