import React, { useMemo, useState } from 'react';
import { useTable, usePagination, useFilters } from 'react-table';
import { Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Link, Paper, TablePagination, Button, TextField } from '@mui/material';
import {Box, Modal, FormControl,DialogActions,  InputLabel, Input} from '@mui/material';
import axios from 'axios';

import SelectColumnFilter from './SelectColumnFilter';

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
  };
  

function EarthquakeList({ data, paginationInfo, fetchData }) {
    const [rowsPerPage, setRowsPerPage] = useState(paginationInfo.itemsPerPage);
    const [pageNumber, setPageNumber] = useState(paginationInfo.currentPage);
    const [filterValues, setFilterValues] = useState({});
    const [showModal, setShowModal] = useState(false);
    const [comment, setComment] = useState('');
    const [selectedEarthquakeId, setSelectedEarthquakeId] = useState(null);


    const handleClickRow = (row, e) => {
        console.log(e)
        if (e.target.tagName.toLowerCase() === 'a') {
            return;
        }
        setSelectedEarthquakeId(row.original.id);
        setShowModal(true);
    };
    const handleSubmitComment = () => {
        // Aquí envías el comentario al servidor junto con el ID del terremoto
        axios.post(`http://localhost:3000/api/features/${selectedEarthquakeId}/comments`, { comment: {body: comment} })
            .then(response => {
                console.log('Comentario enviado:', comment);
                setShowModal(false);
                // Aquí puedes realizar alguna acción adicional después de enviar el comentario
            })
            .catch(error => {
                console.error('Error al enviar el comentario:', error);
                // Maneja cualquier error que ocurra al enviar el comentario
            });
    };

    const handleFilterChange = (obj) => {
        setFilterValues({ mag_type: obj['mag_type'] });
        setRowsPerPage(obj.per_page);
        setPageNumber(obj.page_number);
    };

    const handleChangeRowsPerPage = (event) => {
        const newRowsPerPage = event.target.value || 100;
        setPageNumber(1);
        setFilterValues({ mag_type: filterValues['mag_type'] });
        setRowsPerPage(newRowsPerPage);
        setPageNumber(1);
        fetchData({ per_page: newRowsPerPage, page: 1, mag_type: filterValues['mag_type'] });
    };

    const handleChangePage = (event, newPage) => {
        const adjustedPageNumber = newPage + 1;
        setPageNumber(adjustedPageNumber);
        fetchData({ per_page: rowsPerPage, page: adjustedPageNumber, ...filterValues });
    };

    const totalItems = paginationInfo.totalItems;

    const columns = useMemo(() => [
        { Header: 'Id', accessor: 'id' },
        { Header: 'Id Externo', accessor: 'attributes.external_id' },
        { Header: 'Fecha y Hora', accessor: 'attributes.time' },
        { Header: 'Magnitud', accessor: 'attributes.magnitude' },
        { Header: 'Lugar', accessor: 'attributes.place' },
        { Header: 'URL', accessor: 'attributes.external_url', Cell: ({ value }) => <Link href={value} target="_blank" rel="noopener noreferrer">Ver Detalles</Link> },
        { Header: 'Generó Tsunami', accessor: 'attributes.tsunami', Cell: ({ value }) => value ? 'Sí' : 'No' },
        {
            Header: 'Tipo de Magnitud', accessor: 'attributes.mag_type',
            Filter: ({ column }) => (
                <SelectColumnFilter
                    column={column}
                    fetchData={fetchData}
                    onFilterChange={handleFilterChange}
                    rowsPerPage={rowsPerPage}
                    pageNumber={pageNumber}
                />
            ), filter: 'includes'
        },
        { Header: 'Título', accessor: 'attributes.title' },
        { Header: 'Longitud', accessor: 'attributes.coordinates.longitude' },
        { Header: 'Latitud', accessor: 'attributes.coordinates.latitude' }
    ], [fetchData]);

    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        rows,
        prepareRow,
    } = useTable(
        { columns, data: data, initialState: { pageIndex: paginationInfo.currentPage } },
        useFilters,
        usePagination
    );

    return (
        <div style={{ display: 'flex', justifyContent: 'center', margin: '20px', overflowY: 'auto' }}>
            <TableContainer component={Paper} elevation={3} sx={{ margin: '20px' }}>
                <Table {...getTableProps()} sx={{ minWidth: 650 }} aria-label="simple table">
                    <TableHead>
                        {headerGroups.map(headerGroup => (
                            <tr {...headerGroup.getHeaderGroupProps()} style={{ height: '50px' }}>
                                {headerGroup.headers.map(column => (
                                    <th {...column.getHeaderProps()} style={{ fontWeight: 'bold', minWidth: '10px', whiteSpace: 'nowrap' }}>
                                        <div>{column.render('Header')}</div>
                                        <div>{column.Filter ? column.render('Filter') : null}</div>
                                    </th>
                                ))}
                            </tr>
                        ))}
                    </TableHead>
                    <TableBody {...getTableBodyProps()}>
                        {rows.map(row => {
                            prepareRow(row);
                            return (
                                <TableRow hover sx={{ cursor: 'pointer' }} {...row.getRowProps()} onClick={(e) => handleClickRow(row, e)} >
                                    {row.cells.map(cell => (
                                        <TableCell {...cell.getCellProps()}>{cell.render('Cell')}</TableCell>
                                    ))}
                                </TableRow>
                            );
                        })}
                    </TableBody>
                    <tfoot>
                        <tr>
                            <TablePagination
                                rowsPerPageOptions={[10, 50, 100, 500, 1000]}
                                count={totalItems}
                                rowsPerPage={rowsPerPage}
                                page={pageNumber - 1}
                                onPageChange={handleChangePage}
                                onRowsPerPageChange={handleChangeRowsPerPage}
                                labelRowsPerPage="Filas por página"
                                labelDisplayedRows={({ from, to, count }) => `${from}-${to} de ${count}`}
                            />
                        </tr>
                    </tfoot>
                </Table>
            </TableContainer>

            <Modal open={showModal} onClose={() => setShowModal(false)}>
            <Box
                sx={{
                    position: 'absolute',
                    width: 400,
                    bgcolor: 'background.paper',
                    border: '2px solid #000',
                    boxShadow: 24,
                    p: 4,
                    top: '50%',
                    left: '50%',
                    transform: 'translate(-50%, -50%)',
                }}
            >
                <FormControl fullWidth>
                    <InputLabel htmlFor="comment">Comentario</InputLabel>
                    <Input
                        id="comment"
                        multiline
                        rows={4}
                        value={comment}
                        onChange={(e) => setComment(e.target.value)}
                    />
                </FormControl>
                <DialogActions>
                    <Button onClick={() => setShowModal(false)}>Cancelar</Button>
                    <Button variant="contained" onClick={handleSubmitComment}>Enviar Comentario</Button>
                </DialogActions>
            </Box>
        </Modal>
        </div>
    );
}

export default EarthquakeList;