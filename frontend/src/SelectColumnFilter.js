import React, { useState } from 'react';
import { FormControl, Select, MenuItem } from '@mui/material';

const SelectColumnFilter = ({
  column: { filterValue, setFilter },
  fetchData,
  onFilterChange,
  rowsPerPage,
  pageNumber
}) => {
  const options = ['md', 'ml', 'ms', 'mw', 'me', 'mi', 'mb', 'mlg'];

  // Inicializa el estado con la opción "Todos" solo si no hay ningún valor inicial
  const [selectedValues, setSelectedValues] = useState(filterValue ? filterValue : [""]);

  const handleChange = (event) => {
    const newSelectedValues = event.target.value;

    // Si se selecciona la opción "Todos", activar todos los demás filtros
    if (newSelectedValues[newSelectedValues.length - 1] === "") {
      setSelectedValues([]);
      setFilter([]);
      fetchData({ mag_type: [], per_page: rowsPerPage, page_number: 1 });
      onFilterChange({ mag_type: [], per_page: rowsPerPage, page_number: 1 });
    } else {
      setSelectedValues(newSelectedValues.filter(value => value !== ""));
      setFilter(newSelectedValues.filter(value => value !== ""));
      fetchData({ mag_type: newSelectedValues.filter(value => value !== ""), per_page: rowsPerPage, page_number: 1 });
      onFilterChange({ mag_type: newSelectedValues.filter(value => value !== ""), per_page: rowsPerPage, page_number: 1 });
    }
  };

  return (
    <FormControl fullWidth style={{ marginTop: '5px', alignItems: 'center' }}>
      <Select
        multiple
        style={{ width: '100px' }}
        value={selectedValues}
        onChange={handleChange}
        name='mag_type'
      >
        <MenuItem value="">Todos</MenuItem>
        {options.map((option) => (
          <MenuItem key={option} value={option}>
            {option}
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  );
};

export default SelectColumnFilter;