# API-Earthquake
this is a simple test challenge  earthquake task rake and rest api using DRY basics params validation, serialization json-api principles and ruby on rails.

## Requirements
 - Ruby V 3.3.0
 - Docker-compose (Optional)
 - Docker (Optional)
 - Bundle install

## Local Installation

1. clone the repository
```
git clone git@gitlab.com:yugo132/earthquake_api.git
```
2. navegate to the project folder

```
cd earthquake_api
```
3. bundle install
```
bundle install
```
4. create database and make migrations, *I am using Postgresql with docker-compose just for test but u can change the adapter to local database gems required and editing config/database.yml* and export environment variables for db DB_USERNAME and DB_PASSWORD


```
rails db:create
rails db:migrate

```

5. load earthquake data

```
bundle exec rake earthquake:load_data
```

6. run the server and enjoy testing the api!

```
rails server
```

# Optional (Docker)
add environment files

.postgres.env to postgresql service example:
```
POSTGRES_USER: postgres
POSTGRES_PASSWORD: postgres
POSTGRES_DB: earthquake_db
```

.env to rails service
```
RAILS_ENV=development
DATABASE_URL=postgres://postgres:postgres@db/earthquake_db
RAILS_MASTER_KEY = <your-master-key>
DB_USERNAME=postgres
DB_PASSWORD=postgres
```

With docker and docker-compose just

```
docker-compose build
docker-compose up 
```

and rerun in other terminal the commands of step 4 and 5

```
docker-compose run backend ./bin/rails db:create
docker-compose run backend ./bin/rails db:migrate
docker-compose run backend bundle exec rake earthquake:load_data
```

# Plus (React)
the *frontend/* folder have a datatable interface using react-start-app and react-table UI
is experimental you can try and test build the project and run

```
cd ./frontend
node install
npm start
```


## Contributors
- Ramses Salazar (Yugo)

## License
This project is licensed under the MIT License.

Thanks for read!