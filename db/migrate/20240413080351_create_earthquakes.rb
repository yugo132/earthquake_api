class CreateEarthquakes < ActiveRecord::Migration[7.1]
  def change
    create_table :earthquakes do |t|
      t.string :external_id, null: false
      t.decimal :magnitude, precision: 10, scale: 2, null: false
      t.string :place, null: false
      t.datetime :time
      t.string :url, null: false
      t.boolean :tsunami
      t.string :mag_type, null: false
      t.string :title, null: false
      t.decimal :longitude, precision: 10, scale: 6, null: false
      t.decimal :latitude, precision: 10, scale: 6, null: false
      t.timestamps
    end
    add_index :earthquakes, :external_id, unique: true

  end
end