class Earthquake < ApplicationRecord
    validates :external_id, presence: true, uniqueness: true
    validates :magnitude, numericality: { greater_than_or_equal_to: -1.0, less_than_or_equal_to: 10.0 }, allow_nil: true
    validates :longitude, numericality: { greater_than_or_equal_to: -180.0, less_than_or_equal_to: 180.0 }, allow_nil: true
    validates :latitude, numericality: { greater_than_or_equal_to: -90.0, less_than_or_equal_to: 90.0 }, allow_nil: true
    validates :title, :place, :url, :mag_type, presence: true

    has_many :comments
end
