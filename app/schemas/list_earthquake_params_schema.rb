require 'dry-schema'

class ListEarthquakeParamsSchema < Dry::Schema::Params
    define do
      optional(:mag_type).array(:string).each(included_in?: %w[md ml ms mw me mi mb mlg])
      optional(:page).filled(:integer)
      optional(:per_page).filled(:integer)
    end
  
    def self.call(input)
       new.call(input)
    end
  end