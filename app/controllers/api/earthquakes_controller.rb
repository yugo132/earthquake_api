module Api
  class EarthquakesController < ApplicationController
    before_action :validate_params, only: :index

    def index
      pagy, earthquakes = fetch_earthquakes

      render json: {
        data: earthquakes.map { |earthquake| EarthquakeSerializer.new(earthquake).serializable_hash.dig(:data) },
        pagination: {
          current_page: pagy.page,
          total: pagy.count,
          per_page: pagy.items
        }
      }
    end

    private

    def index_params
      params.permit(:page, :per_page, mag_type: []).to_h
    end

    def validate_params
      result = ListEarthquakeParamsSchema.call(index_params)

      return if result.success?

      render json: { error: result.errors.to_h }, status: :unprocessable_entity
    end

    def fetch_earthquakes
      earthquakes = Earthquake.all

      mag_types = params[:mag_type].presence || []
      earthquakes = earthquakes.where(mag_type: mag_types) if mag_types.any?
      earthquakes = earthquakes.order(time: :desc)
      pagy(earthquakes, page: params[:page], items: params[:per_page] || 100)
    end
  end
end