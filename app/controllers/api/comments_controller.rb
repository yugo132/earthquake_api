module Api
  class CommentsController < ApplicationController
    before_action :set_earthquake

    def create
      comment = @earthquake.comments.build(comment_params)
      if comment.save
        render json: comment, status: :created
      else
        render json: comment.errors, status: :unprocessable_entity
      end
    end
  
    private
  
    def set_earthquake
      @earthquake = Earthquake.find_by(id: params[:earthquake_id])
  
      unless @earthquake
        render json: { error: 'Earthquake not found' }, status: :not_found
      end
    end
  
    def comment_params
      params.require(:comment).permit(:body)
    end
  end
end