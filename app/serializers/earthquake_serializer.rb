class EarthquakeSerializer
    include JSONAPI::Serializer
  
    set_type :feature
    attributes :external_id, :magnitude, :place, :time, :tsunami, :mag_type, :title
    attribute :coordinates do |earthquake|
      { longitude: earthquake.longitude, latitude: earthquake.latitude }
    end
    attribute :external_url, &:url
end