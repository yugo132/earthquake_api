require 'colorize'

namespace :earthquake do
    desc "Load earthquake data from USGS API"
    task load_data: :environment do
      require 'net/http'
      require 'json'
      require 'pp'
  
      url = URI("https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/all_month.geojson")
      response = Net::HTTP.get(url)
      data = JSON.parse(response)

      total_records = data["features"].size
      records_saved = 0
      records_skipped = 0
  
      data["features"].each do |feature|
        properties = feature["properties"]
        next if properties["title"].nil? || properties["url"].nil? || properties["place"].nil? || properties["magType"].nil? || feature["geometry"]["coordinates"].nil?

        time = properties["time"].nil? ? nil : Time.at(properties["time"] / 1000)


        earthquake = Earthquake.new(
          external_id: feature["id"],
          magnitude: properties["mag"],
          place: properties["place"],
          time: time,
          url: properties["url"],
          tsunami: properties["tsunami"],
          mag_type: properties["magType"],
          title: properties["title"],
          longitude: feature["geometry"]["coordinates"][0],
          latitude: feature["geometry"]["coordinates"][1]
        )
  
        if earthquake.valid?
          earthquake.save
          records_saved += 1

        else
          error_message = "Error - Record Skip: #{earthquake.errors.full_messages.join(', ')}"
          puts error_message.colorize(:red)
          pp earthquake
          records_skipped += 1
        end
      end

      puts "Total earthquakes founds: #{total_records}"
      puts "Total earthquakes saved: #{records_saved}"
      puts "Total earthquakes skip: #{records_skipped}"
    end
  end
  